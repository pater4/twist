module.exports =
  windowSize:
    width: 1440
    height: 850
  assets: [
    'assets/map0.png'
    'assets/char.png'
    'assets/dg.png'
    'assets/bg.png'
    'assets/object.png'
    'assets/NostalgicSong.MP3'
    'assets/PlacesofSoul.MP3'
    'assets/WorldofMana.MP3'
  ]
  fps: 30
  nodeType:
    core     : 'core'
    scene    : 'scene'
    key      : 'scene/key'
    bg       : 'scene/bg'
    dungeon  : 'scene/dungeon'
    hud      : 'scene/hud'
