Config = require 'Config'
# Scene = require 'Scene/Splash/Scene'
Scene = require 'Scene/Stage/Stage1/Scene'

enchant()

window.onload = ->
  core = new Core(Config.windowSize.width, Config.windowSize.height)
  core.fps = Config.fps
  core.preload Config.assets

  core.onload = ->
    core.pushScene new Scene()

  core.start()
