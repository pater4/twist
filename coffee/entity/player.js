var Player = enchant.Class.create(enchant.Label, {
    initialize: function(x, y) {
        enchant.Label.call(this)
        this.text = "人"
        this.backgroundColor = "#ff8888"
        this.width = 32
        this.height = 32
        this.x = x
        this.y = y
        this.walls = []
        this.boxs = []
        this.font = "32px 'Arial'"
        Dispatcher.addNode(this)
    },

    onKEY_LEFT_START: function(){
        if (!this.canWalkTo('left'))
            return this.push('left')
        this.walk('left')
    },

    onKEY_RIGHT_START: function(){
        if (!this.canWalkTo('right'))
            return this.push('right')
        this.walk('right')
    },

    onKEY_UP_START: function(){
        if (!this.canWalkTo('up'))
            return this.push('up')
        this.walk('up')
    },

    onKEY_DOWN_START: function(){
        if (!this.canWalkTo('down'))
            return this.push('down')
        this.walk('down')
    },

    onBOX_MOVED: function(event){
        this.boxs = _.reject(this.boxs, function (box) {
            return box.x == event.oldX && box.y == event.oldY
        })
        this.boxs.push({x: event.x, y: event.y})
    },

    onREADY: function(event){
        this.boxs = event.boxs
        this.walls = event.walls
    },

    push: function(direct){
        var pos = this.calcPos(direct)
        Dispatcher.trigger(Dispatcher.createEvent('PLAYER_PUSHED', {direct: direct, x: pos.x, y: pos.y}))
    },

    walk: function(direct){
        var pos = this.calcPos(direct)
        this.moveTo(pos.x, pos.y)
        // this.tl.moveTo(pos.x, pos.y, 6, enchant.Easing.LINEAR)
        Dispatcher.trigger(Dispatcher.createEvent('PLAYER_WALKED', {x: pos.x, y: pos.y}))
    },

    canWalkTo: function(direct){
        var pos = this.calcPos(direct)
        var calcResult = function (result, entity) {
            return result && (entity.x != pos.x || entity.y != pos.y)
        }
        var wallResult = _.chain(this.walls).reduce(calcResult, true).value()
        var boxResult =  _.chain(this.boxs).reduce(calcResult, true).value()
        return wallResult && boxResult
    },

    calcPos: function(direct){
        var x = this.x, y = this.y
        switch (direct) {
            case 'up':
                y -= this.height
                break;
            case 'down':
                y += this.height
                break;
            case 'left':
                x -= this.width
                break;
            case 'right':
                x += this.width
                break;
            default :
        }

        return {x: x, y:y}
    },
})
