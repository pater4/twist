var Fps = enchant.Class.create(enchant.Label, {
    initialize: function() {
        enchant.Label.call(this)
        // フレームレート
        this.total = 0
        this.count = 0
        this.moveTo(20, 20)
    },

    onenterframe: function(e) {
        this.total += e.elapsed
        this.count++
        // 1秒 (1000ミリ秒) 以上経過していた場合、FPSの平均値を求める
        if ( this.total >= 1000 ) {
            this.text = (1000 / this.total * this.count).toFixed(2) + " FPS"
            // それぞれリセットする
            this.total = 0
            this.count = 0
        }
    },
})

var DebugLayer = enchant.Class.create(enchant.Group, {
    initialize: function() {
        enchant.Group.call(this)
        this.addChild(new Fps())
    },
})
