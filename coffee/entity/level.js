var Level = enchant.Class.create(enchant.Map, {
    initialize: function(asset, data) {
        var self = this
        enchant.Map.call(this, 32, 32)
        this.image = Common.core.assets[asset]
        this.loadData(data)
        this.walls = _.chain(data).map(function (types, y) {
            return _.map(types, function (type, x) {
                return {x: x * self.tileWidth, y: y * self.tileHeight, type: type}
            })
        }).flatten(true)
        .filter(function (val) { return val.type == 1}).value()
    },
})
