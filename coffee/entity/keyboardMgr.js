var KeyboardMgr = enchant.Class.create(enchant.Entity, {
    initialize: function() {
        enchant.Entity.call(this)
        Common.core.keybind(' '.charCodeAt(0), 'a')
        Common.core.keybind('s'.charCodeAt(0), 'b')
        Common.core.keybind('d'.charCodeAt(0), 'c')

        Common.core.on('enterframe', function() {
            if (Common.core.input.up) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_UP_KEEP'))
            }
            if (Common.core.input.down) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_DOWN_KEEP'))
            }
            if (Common.core.input.right) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_RIGHT_KEEP'))
            }
            if (Common.core.input.left) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_LEFT_KEEP'))
            }
            if (Common.core.input.a) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_A_KEEP'))
            }
            if (Common.core.input.b) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_B_KEEP'))
            }
            if (Common.core.input.c) {
                Dispatcher.trigger(Dispatcher.createEvent('KEY_C_KEEP'))
            }
        })

        Common.core.on('upbuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_UP_START'))
        })
        Common.core.on('upbuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_UP_END'))
        })
        Common.core.on('downbuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_DOWN_START'))
        })
        Common.core.on('downbuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_DOWN_END'))
        })
        Common.core.on('leftbuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_LEFT_START'))
        })
        Common.core.on('leftbuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_LEFT_END'))
        })
        Common.core.on('rightbuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_RIGHT_START'))
        })
        Common.core.on('rightbuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_RIGHT_END'))
        })
        Common.core.on('abuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_A_START'))
        })
        Common.core.on('abuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_A_END'))
        })
        Common.core.on('bbuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_B_START'))
        })
        Common.core.on('bbuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_B_END'))
        })
        Common.core.on('cbuttondown', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_C_START'))
        })
        Common.core.on('cbuttonup', function() {
            Dispatcher.trigger(Dispatcher.createEvent('KEY_C_END'))
        })
    },
})
