var TransitionMgr = enchant.Class.create(enchant.Entity, {
    initialize: function() {
        enchant.Entity.call(this)
        Common.core.on('STAGE_CLEARED', function(event) {
            switch (event.stage) {
                case 1:
                    Common.core.replaceScene(stage2())
                    break
                case 2:
                    Common.core.replaceScene(topScene())
                    break
                default:
            }
        })
        Common.core.on('STAGE_RESET', function(event) {
            switch (event.stage) {
                case 1:
                    Common.core.replaceScene(stage1())
                    break
                case 2:
                    Common.core.replaceScene(stage2())
                    break
                default:
            }
        })
        Common.core.on('GAME_STARTED', function(event) {
            Common.core.replaceScene(stage1())
        })
        Common.core.on('SPLASHED', function(event) {
            Common.core.replaceScene(topScene())
        })
    },
})
