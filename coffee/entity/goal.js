var Goal = enchant.Class.create(enchant.Label, {
    initialize: function(x, y) {
        enchant.Label.call(this)
        this.text = "□"
        this.backgroundColor = "#8888ff"
        this.width = 32
        this.height = 32
        this.x = x
        this.y = y
        this.walls = []
        this.font = "32px 'Arial'"
        Dispatcher.addNode(this)
    },

    onREADY: function(event){
        this.boxs = event.boxs
    },

    onBOX_MOVED: function(event){
        if (event.x != this.x || event.y != this.y) return
        this.goalMatchBox()
    },

    goalMatchBox: function(){
        Dispatcher.trigger(Dispatcher.createEvent('GOAL_MATCHED_BOX', {x: this.x, y: this.y}))
    },
})
