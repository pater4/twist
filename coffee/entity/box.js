var Box = enchant.Class.create(enchant.Label, {
    initialize: function(x, y) {
        enchant.Label.call(this)
        this.text = "箱"
        this.backgroundColor = "#ffcc88"
        this.width = 32
        this.height = 32
        this.x = x
        this.y = y
        this.walls = []
        this.boxs = []
        this.lock = false
        this.font = "32px 'Arial'"
        Dispatcher.addNode(this)
    },

    onREADY: function(event){
        this.boxs = event.boxs
        this.walls = event.walls
    },

    onPLAYER_PUSHED: function(event){
        if (event.x != this.x || event.y != this.y) return
        if (!this.canMoveTo(event.direct)) return
        this.moveToDirct(event.direct)
    },

    onGOAL_MATCHED_BOX: function(event){
        if (event.x != this.x || event.y != this.y) return
        this.lock = true;
    },

    moveToDirct: function(direct){
        var pos = this.calcPos(direct)
        var oldX = this.x, oldY = this.y
        this.moveTo(pos.x, pos.y)
        //this.tl.moveTo(pos.x, pos.y, 6, enchant.Easing.LINEAR)
        Dispatcher.trigger(Dispatcher.createEvent('BOX_MOVED', {x: pos.x, y: pos.y, oldX: oldX, oldY: oldY}))
    },

    canMoveTo: function(direct){
        var pos = this.calcPos(direct)
        return (this.lock == false) && _.chain(this.walls).reduce(function (result, wall) {
            return result && (wall.x != pos.x || wall.y != pos.y)
        }, true).value()
    },

    calcPos: function(direct){
        var x = this.x, y = this.y
        switch (direct) {
            case 'up':
                y -= this.height
                break;
            case 'down':
                y += this.height
                break;
            case 'left':
                x -= this.width
                break;
            case 'right':
                x += this.width
                break;
            default :
        }

        return {x: x, y:y}
    },
})
