Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = ''
    @_element = document.createElement 'div'
    @_element.id = 'logo_bg'
    @width = Config.windowSize.width
    @height = Config.windowSize.height
    @x = 0
    @y = 0
