Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = 'Please space key'
    @_element = document.createElement 'div'
    @_element.id = 'top_start_label'
    @width = Config.windowSize.width
    @height = null
    @x = 0
    @y = 350
    @font = '24px \'Arial\''
    @textAlign = 'center'
