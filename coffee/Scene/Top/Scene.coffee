Scene  = require 'Core/Base/Scene'
Config = require 'Config'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/WorldofMana.MP3'
    StartLabel = require './StartLabel'
    @hud.addChild new StartLabel()
    @NextScene = require 'Scene/Stage/Stage1/Scene'
    @addOperationTarget @

  onPauseButtonDown: ->
    @replaceScene new @NextScene()
