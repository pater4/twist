Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = '制作'
    @_element = document.createElement 'div'
    @_element.id = 'game_over_label'
    @width = null
    @height = null
    @x = 0
    @y = 0
    @font = '30px \'Arial\''
    @textAlign = 'center'
