Scene         = require 'Core/Base/Scene'
Config        = require 'Config'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/NostalgicSong.MP3'
    @GraphicLabel = require './GraphicLabel'
    @SystemLabel = require './SystemLabel'
    @MpLabel = require './MpLabel'
    @SpecialThanksLabel = require './SpecialThanksLabel'
    @ProductionLabel = require './ProductionLabel'
    @NextScene = require 'Scene/Top/Scene'

    @tl
      .delay 30
      .then ->
        @hud.addChild new @GraphicLabel()
      .delay 30
      .then ->
        @hud.addChild new @SystemLabel()
      .delay 30
      .then ->
        @hud.addChild new @MpLabel()
      .delay 30
      .then ->
        @hud.addChild new @SpecialThanksLabel()
      .delay 30
      .then ->
        @hud.addChild new @ProductionLabel()
      .delay 30
      .then ->
        @replaceScene new @NextScene()
