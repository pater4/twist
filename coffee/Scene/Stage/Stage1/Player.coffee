Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance

    @x = 128
    @y = 128
    @width = 128
    @height = 128
    @image = core.assets['assets/char.png']
    @frame = 0
    @gravity = 25
    @jampPower = 60
    @jampResistivity = 4
    @xAccel = 25
    @yAccel = 0
    @landing = false
    @count = 0
    @rotateTime = 15
    @rotating = false
    @rect =
      x: 43
      y: 9
      w: 42
      h: 119

  onBButtonDown: ->
    if @rotating
      return
    @jump()

  onLButtonDown: ->
    @twist('L')

  onRButtonDown: ->
    @twist('R')

  onenterframe: ->
    if @rotating
      return
    @kickWall()
    @run()
    @fall()
    @motion()
    # @replaceScene new @NextScene()

  fall: ->
    @y += @gravity - @yAccel
    max = @collisionsCheck 'y'
    @landing = false
    if max != null
      @y = max
      @y += 1
      max = @collisionsCheck 'y'
      @y -= 1
      if max != null
        @landing = true

    @yAccel -= @jampResistivity
    if @yAccel < 3
      @yAccel = 0

  run: () ->
    currentScene = enchant.Core.instance.currentScene
    axisX = currentScene.controller.input.AxisX
    if -0.5 < axisX < 0.5
      return
    @x += axisX * @xAccel
    max = @collisionsCheck 'x'
    if max != null
      @x = max

  jump: () ->
    if @yAccel != 0 or !@landing
      return
    @count = 0
    @yAccel = @jampPower

  kickWall: () ->
    currentScene = enchant.Core.instance.currentScene
    axisX = currentScene.controller.input.AxisX
    if -0.5 < axisX < 0.5 or @landing
      return
    @count = 0
    if axisX > 0
      @kick = -2
    else
      @kick = 2
    @x += @kick
    max = @collisionsCheck 'x'
    if max != null
      @yAccel = @jampPower
    @x -= @kick

  motion: () ->
    currentScene = enchant.Core.instance.currentScene
    axisX = currentScene.controller.input.AxisX
    frame = Math.floor(@count / 7)
    @frame = frame % 4

    if  (axisX >= 0.5 or -0.5 >= axisX) and @landing
      frame = @count
      @frame = frame % 5 + 5

    if  axisX >= 0.5
      @scaleX = 1

    if  -0.5 >= axisX
      @scaleX = -1

    if  !@landing
      @frame = 11

    @count++
    if (@count > 100000)
      @count = 0

  twist: () ->
    if @rotating
      return
    @rotating = true
    @tl.delay @rotateTime
      .then =>
        @rotating = false

  getBound: ->
    x : @x + @rect.x
    y : @y + @rect.y
    xw: @x + @rect.x + @rect.w
    yh: @y + @rect.y + @rect.h

  collisionsCheck: (direct)->
    currentScene = enchant.Core.instance.currentScene
    max = currentScene.dungeon.collisionsCheck @, direct
    max2 = currentScene.boxs.collisionsCheck @, direct
    max || max2
