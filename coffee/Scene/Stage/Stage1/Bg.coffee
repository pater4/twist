Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance

    @x = 0
    @y = 0
    @width = Config.windowSize.width
    @height = Config.windowSize.height
    @image = core.assets['assets/bg.png']
