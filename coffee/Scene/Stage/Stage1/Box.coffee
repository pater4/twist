Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: (x, y) ->
    enchant.Sprite.call @
    core = enchant.Core.instance

    @x = x * 128
    @y = y * 128
    @width = 128
    @height = 128
    @image = core.assets['assets/object.png']
    @frame = 0
    @gravity = 25
    @landing = false
    @count = 0
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128

  onenterframe: ->
    if @visible
      @fall()
      @motion()

  slide: ->
    max = @collisionsCheck 'x'
    if max != null
      @x = max

  fall: ->
    @y += @gravity
    max = @collisionsCheck 'y'
    @landing = false
    if max != null
      @y = max
      @y += 1
      max = @collisionsCheck 'y'
      @y -= 1
      if max != null
        @landing = true

  motion: () ->
    @count++
    if (@count > 100000)
      @count = 0

  getBound: ->
    x : @x + @rect.x
    y : @y + @rect.y
    xw: @x + @rect.x + @rect.w
    yh: @y + @rect.y + @rect.h

  collisionsCheck: (direct)->
    currentScene = enchant.Core.instance.currentScene
    max = currentScene.dungeon.collisionsCheck @, direct
    max2 = currentScene.boxs.collisionsCheck @, direct
    max || max2
