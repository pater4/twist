Config = require 'Config'

module.exports = enchant.Class.create enchant.Group,
  initialize: () ->
    enchant.Group.call @
    @rotateTime = 15
    @rotating = false

  collisionCheck: (box, node, direct) ->
    if box == node
      null
    else
      nodeBound = node.getBound()
      boxBound = box.getBound()

      if nodeBound.xw <= boxBound.x or nodeBound.yh <= boxBound.y or nodeBound.x >= boxBound.xw or nodeBound.y >= boxBound.yh
        null
      else
        switch direct
          when 'y'
            if nodeBound.yh > boxBound.y and nodeBound.y < boxBound.y
              boxBound.y - (node.rect.y + node.rect.h)
            else
              boxBound.yh - node.rect.y
          else
            if boxBound.xw > boxBound.x and nodeBound.x < boxBound.x
              boxBound.x - (node.rect.x + node.rect.w)
            else
              boxBound.xw - node.rect.x

  collisionsCheck: (node, direct) ->
    checkPoint = if direct == 'x' then node.x else node.y
    _.reduce @childNodes, (result, child) =>
      col = @collisionCheck child, node, direct
      if Math.abs checkPoint - col > Math.abs checkPoint - result
        result
      else
        col
    , null

  onLButtonDown: ->
    @twist('L')

  onRButtonDown: ->
    @twist('R')

  twist: (rotate) ->
    if @rotating
      return
    @rotating = true
    _.each @childNodes, (node) =>
      node.visible = false
    @tl.delay @rotateTime
      .then =>
        @twistNodes rotate
        _.each @childNodes, (node) =>
          node.visible = true
        @rotating = false

  twistNode: (node, rotate) ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.dungeon
      return

    map = currentScene.dungeon.map
    size = currentScene.dungeon.size
    originY = originX = (map.length * size / 2) - (size / 2)
    px = node.x - originX
    py = -(node.y - originY)

    switch rotate
      when 'L'
        r = 90 * Math.PI / 180;
        x = Math.cos(r) * px - Math.sin(r) * py;
        y = Math.cos(r) * py + Math.sin(r) * px;
      else
        r = 270 * Math.PI / 180;
        x = Math.cos(r) * px - Math.sin(r) * py;
        y = Math.cos(r) * py + Math.sin(r) * px;
    node.x = Math.round(originX + x)
    node.y = Math.round(originY + -y)

  twistNodes: (rotate) ->
    _.each @childNodes, (node) =>
      @twistNode node, rotate
