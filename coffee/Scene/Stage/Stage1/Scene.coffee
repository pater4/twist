Scene  = require 'Core/Base/Scene'
Config = require 'Config'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/WorldofMana.MP3'
    Bg = require './Bg'
    @bg.addChild new Bg()
    Dungeon = require './Dungeon'
    @dungeon = new Dungeon()
    @addOperationTarget @dungeon
    @dg.addChild @dungeon
    # @NextScene = require 'Scene/Stage/Stage2/Scene'

    BoxList = require './BoxList'
    @boxs = new BoxList()
    @dg.addChild @boxs
    @addOperationTarget @boxs
    Box = require './Box'
    @boxs.addChild new Box(1, 3)
    @boxs.addChild new Box(2, 1)
    @boxs.addChild new Box(2, 2)

    Player = require './Player'
    @player = new Player()
    @dg.addChild @player
    @addOperationTarget @player

  onPauseButtonDown: ->
    # @replaceScene new @NextScene()
