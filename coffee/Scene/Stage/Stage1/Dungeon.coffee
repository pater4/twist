Config = require 'Config'

module.exports = enchant.Class.create enchant.Group,
  initialize: () ->
    enchant.Group.call @

    @map = [
      ['e', 't', 't', 't', 't', 't', 't', 't', 't', 't', 't']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'b']
      ['l', ' ', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b']
      ['l', ' ', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b']
      ['v', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b']
    ]

    if @map.length != @map[0].length
      alert '縦:' + @map.length + ' 横:@map[0].length'

    @size = 128
    @createWall @map
    @rotateTime = 15
    @rotating = false

  createWall: (map) ->
    _.each @childNodes, (node) =>
      @removeChild node
    core = enchant.Core.instance
    _.each map, (rowMap, y) =>
      _.each rowMap, (data, x) =>
        switch data
          when 't'
            frame = 1
          when 'b'
            frame = 9
          when 'l'
            frame = 4
          when 'r'
            frame = 6
          when 'e'
            frame = 0
          when 'y'
            frame = 2
          when 'v'
            frame = 8
          when 'n'
            frame = 10
          else
            frame = 7

        wall = new enchant.Sprite(@size, @size)
        wall.frame = frame
        wall.x = x * @size
        wall.y = y * @size
        wall.image = core.assets['assets/dg.png']

        @addChild wall

  collisionCheck: (mapX, mapY, node, direct) ->
    data = @map[mapY] && @map[mapY][mapX]
    wallBound = {x:0, y:0, xw:0, yh:0}
    switch data
      when 't', 'l', 'r', 'e', 'y', 'v', 'n'
        wallBound.x = mapX * @size
        wallBound.y = mapY * @size
        wallBound.xw = wallBound.x + @size
        wallBound.yh = wallBound.y + @size
      when 'b'
        wallBound.x = mapX * @size
        wallBound.y = mapY * @size
        wallBound.xw = wallBound.x + @size
        wallBound.yh = wallBound.y + @size
      else
        return null
    nodeBound = node.getBound()
    if nodeBound.xw <= wallBound.x or nodeBound.yh <= wallBound.y or nodeBound.x >= wallBound.xw or nodeBound.y >= wallBound.yh
      null
    else
      switch direct
        when 'y'
          if nodeBound.yh > wallBound.y and nodeBound.y < wallBound.y
            wallBound.y - (node.rect.y + node.rect.h)
          else
            wallBound.yh - node.rect.y
        else
          if nodeBound.xw > wallBound.x and nodeBound.x < wallBound.x
            wallBound.x - (node.rect.x + node.rect.w)
          else
            wallBound.xw - node.rect.x

  collisionsCheck: (node, direct) ->
    mapX = Math.floor (node.x / 128)
    mapY = Math.floor (node.y / 128)
    checkPoint = if direct == 'x' then node.x else node.y
    _.reduce _.range(-1, 2), (result, x) =>
      col = _.reduce _.range(-1, 2), (result, y) =>
        col = @collisionCheck mapX + x, mapY + y, node, direct
        if Math.abs checkPoint - col > Math.abs checkPoint - result
          result
        else
          col
      , null
      if Math.abs checkPoint - col > Math.abs checkPoint - result
        result
      else
        col
    , null

  onLButtonDown: ->
    @twist('L')

  onRButtonDown: ->
    @twist('R')

  twist: (rotate) ->
    if @rotating
      return
    @rotating = true
    switch rotate
      when 'L'
        @tl.rotateBy -90, @rotateTime
          .then =>
            @twistNode 'L'
            @map = @twistMapCreate 'L'
            @parentNode.followPlayer()
            @rotation = 0
            @rotating = false
      when 'R'
        @tl.rotateBy 90, @rotateTime
          .then =>
            @twistNode 'R'
            @map = @twistMapCreate 'R'
            @parentNode.followPlayer()
            @rotation = 0
            @rotating = false

  onenterframe: ->
    @followPlayer()

  followPlayer: ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.player
      return

    px = currentScene.player.x
    py = currentScene.player.y
    pw = currentScene.player.width
    ph = currentScene.player.height
    @originX = px + (pw / 2)
    @originY = py + (ph / 2)

  twistMapCreate: (rotate)->
    transpose = (map) ->
        return _.map _(map[0]).keys().reverse(), (x) ->
          return _.map map, (row) ->
            return row[x]
    transposeR = (map) ->
        reverseMap = _(map).reverse()
        return _.map _(reverseMap[0]).keys(), (x) ->
          return _.map reverseMap, (row) ->
            return row[x]
    switch rotate
      when 'L'
        @map = transpose(@map)
        @createWall @map
      else
        @map = transposeR(@map)
        @createWall @map

  twistNode: (rotate) ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.player
      return
    player = currentScene.player

    originY = originX = (@map.length * @size / 2) - (@size / 2)
    px = player.x - originX
    py = -(player.y - originY)

    switch rotate
      when 'L'
        r = 90 * Math.PI / 180;
        x = Math.cos(r) * px - Math.sin(r) * py;
        y = Math.cos(r) * py + Math.sin(r) * px;
      else
        r = 270 * Math.PI / 180;
        x = Math.cos(r) * px - Math.sin(r) * py;
        y = Math.cos(r) * py + Math.sin(r) * px;
    currentScene.player.x = Math.round(originX + x)
    currentScene.player.y = Math.round(originY + -y)
