Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = 'Game Over'
    @_element = document.createElement 'div'
    @_element.id = 'game_over_label'
    @width = Config.windowSize.width
    @height = null
    @x = 0
    @y = 200
    @font = '60px \'Arial\''
    @textAlign = 'center'
