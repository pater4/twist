Scene         = require 'Core/Base/Scene'
System        = require 'Core/System'
Common        = require 'Core/Common'
Config        = require 'Config'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/NostalgicSong.MP3'
    GameOverLabel = require './GameOverLabel'
    System.nodeOf new GameOverLabel(), Config.nodeType.hud, @hud
    @NextScene = require 'Scene/Top/Scene'

  onenterframe: ->
    @tl
      .delay 60
      .then ->
        Common.core.replaceScene new @NextScene()
