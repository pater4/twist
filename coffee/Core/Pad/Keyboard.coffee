module.exports = enchant.Class.create enchant.Entity,
  initialize: () ->
    enchant.Entity.call @
    core = enchant.Core.instance
    @nodeList = []

    core.keybind ' '.charCodeAt(0), 'Pause'
    core.keybind 'a'.charCodeAt(0), 'L'
    core.keybind 's'.charCodeAt(0), 'A'
    core.keybind 'w'.charCodeAt(0), 'B'
    core.keybind 'd'.charCodeAt(0), 'R'
    _.each ['Pause', 'L', 'A', 'B', 'R'], (button) =>
      core.clearEventListener button + 'buttondown'
      core.clearEventListener button + 'buttonup'

      core.on button + 'buttonup', =>
        _.each @nodeList, (node) ->
          event = new enchant.Event button + 'ButtonUp'
          node.dispatchEvent event

      core.on button + 'buttondown', =>
        _.each @nodeList, (node) ->
          event = new enchant.Event button + 'ButtonDown'
          node.dispatchEvent event

  onenterframe: ->
    core = enchant.Core.instance
    @parentNode.input = {
      AxisX: 0
      AxisY: 0
      A    : false
      B    : false
      L    : false
      R    : false
      Pause: false
    }
    if core.input.up
      @parentNode.input.AxisY = -1

    if core.input.down
      @parentNode.input.AxisY = 1

    if core.input.right
      @parentNode.input.AxisX = 1

    if core.input.left
      @parentNode.input.AxisX = -1

    if core.input.A
      @parentNode.input.A = true

    if core.input.B
      @parentNode.input.B = true

    if core.input.L
      @parentNode.input.L = true

    if core.input.R
      @parentNode.input.R = true

    if core.input.Pause
      @parentNode.input.Pause = true
