GamePad = enchant.Class.create enchant.Entity,
  initialize: () ->
    enchant.Entity.call @
    @nodeList = []

    if !GamePad.oldInput
      GamePad.oldInput = {
        AxisX: 0
        AxisY: 0
        A    : false
        B    : false
        L    : false
        R    : false
        Pause: false
      }

    GamePad.buttons =
      'Pause': 11
      'L'    : 4
      'A'    : 3
      'B'    : 2
      'R'    : 5

  onenterframe: ->
    gamePad = navigator.getGamepads and navigator.getGamepads()[0]
    GamePad.input = {
      AxisX: 0
      AxisY: 0
      A    : false
      B    : false
      L    : false
      R    : false
      Pause: false
    }

    buttons = GamePad.buttons

    if !gamePad
      return

    GamePad.input.AxisX = gamePad.axes[0]
    GamePad.input.AxisY = gamePad.axes[1]

    if gamePad.buttons[buttons.A].pressed
      GamePad.input.A = true

    if gamePad.buttons[buttons.B].pressed
      GamePad.input.B = true

    if gamePad.buttons[buttons.L].pressed
      GamePad.input.L = true

    if gamePad.buttons[buttons.R].pressed
      GamePad.input.R = true

    if gamePad.buttons[buttons.Pause].pressed
      GamePad.input.Pause = true

    # 押された時、離された時のイベント
    _.each buttons, (button, key) =>
      if !GamePad.oldInput[key] and GamePad.input[key]
        _.each @nodeList, (node) ->
          event = new enchant.Event key + 'ButtonDown'
          node.dispatchEvent event

      if GamePad.oldInput[key] and !GamePad.input[key]
        _.each @nodeList, (node) ->
          event = new enchant.Event key + 'ButtonUp'
          node.dispatchEvent event

    GamePad.oldInput = GamePad.input

module.exports = GamePad
