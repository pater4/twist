System = require 'Core/System'
Config = require 'Config'
Common = require 'Core/Common'

module.exports = enchant.Class.create enchant.Entity,
  initialize: ->
    enchant.Entity.call @
    Common.core.keybind ' '.charCodeAt(0), 'a'
    Common.core.keybind 's'.charCodeAt(0), 'b'
    Common.core.keybind 'd'.charCodeAt(0), 'c'

    Common.core.on 'enterframe', ->
      event = null
      if Common.core.input.up
        event = System.createEvent 'KEY_UP_KEEP'

      if Common.core.input.down
        event = System.createEvent 'KEY_DOWN_KEEP'

      if Common.core.input.right
        event = System.createEvent 'KEY_RIGHT_KEEP'

      if Common.core.input.left
        event = System.createEvent 'KEY_LEFT_KEEP'

      if Common.core.input.a
        event = System.createEvent 'KEY_A_KEEP'

      if Common.core.input.b
        event = System.createEvent 'KEY_B_KEEP'

      if Common.core.input.c
        event = System.createEvent 'KEY_C_KEEP'

      if event
        System.trigger event, Config.nodeType.scene

    Common.core.on 'downbuttondown', ->
      event = System.createEvent 'KEY_DOWN_START'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'downbuttonup', ->
      event = System.createEvent 'KEY_DOWN_END'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'leftbuttondown', ->
      event = System.createEvent 'KEY_LEFT_START'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'leftbuttonup', ->
      event = System.createEvent 'KEY_LEFT_END'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'rightbuttondown', ->
      event = System.createEvent 'KEY_RIGHT_START'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'rightbuttonup', ->
      event = System.createEvent 'KEY_RIGHT_END'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'abuttondown', ->
      event = System.createEvent 'KEY_A_START'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'abuttonup', ->
      event = System.createEvent 'KEY_A_END'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'bbuttondown', ->
      event = System.createEvent 'KEY_B_START'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'bbuttonup', ->
      event = System.createEvent 'KEY_B_END'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'cbuttondown', ->
      event = System.createEvent 'KEY_C_START'
      System.trigger event, Config.nodeType.scene

    Common.core.on 'cbuttonup', ->
      event = System.createEvent 'KEY_C_END'
      System.trigger event, Config.nodeType.scene
