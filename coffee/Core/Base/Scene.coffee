Config   = require 'Config'
Dg   = require './DungeonGround'
Keyboard = require 'Core/Pad/Keyboard'
GamePad = require 'Core/Pad/GamePad'

module.exports = enchant.Class.create enchant.Scene,
  initialize: (bgm = '') ->
    enchant.Scene.call @

    @bg = new enchant.Group
    @addChild @bg

    @dg = new Dg
    @addChild @dg

    # BGM設定
    core = enchant.Core.instance
    @bgm = if bgm != '' then core.assets[bgm].clone() else null
    if @bgm
      @on 'enterframe', ->
        @bgm.play()

    # DIVタグ描画用
    @hud = new enchant.Group
    @hud._element = document.createElement 'div'
    @addChild @hud

    # コントローラ設定
    if navigator.getGamepads and navigator.getGamepads()[0]
      @key = new GamePad()
      @controller = GamePad
    else
      @key = new Keyboard()
      @controller = Keyboard
    @addChild @key

  addOperationTarget: (node) ->
    @key.nodeList.push node

  replaceScene: (scene) ->
    @remove()
    if @bgm
      @bgm.volume = 0
      @bgm.stop()
    enchant.Core.instance.replaceScene scene
