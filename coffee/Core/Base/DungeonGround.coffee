Config = require 'Config'

module.exports = enchant.Class.create enchant.Group,
  initialize: () ->
    enchant.Group.call @

  onenterframe: ->
    @followPlayer()

  followPlayer: ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.player
      return

    px = currentScene.player.x
    py = currentScene.player.y
    pw = currentScene.player.width
    ph = currentScene.player.height
    maxW = Config.windowSize.width
    maxH = Config.windowSize.height
    moveX = maxW / 2 - px - pw
    moveY = maxH / 2 - py
    @x = moveX
    @y = moveY
