module.exports = enchant.Class.create enchant.Label,
  initialize: (x, y, width, height) ->
    enchant.Label.call @, x, y

    @width = width || null
    @height = height || null
    @_element = document.createElement 'div'
