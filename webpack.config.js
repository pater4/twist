module.exports = {
  entry: __dirname + "/coffee/Main",
  output: {
    path: __dirname + '/',
    filename: 'app.js'
  },

  module: {
    loaders: [
      {
        test: /\.coffee$/,
        exclude: /node_modules/,
        loader: 'coffee-loader'
      }
    ]
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['', '.js', '.coffee'],
    modulesDirectories: ['coffee']
  }
};
